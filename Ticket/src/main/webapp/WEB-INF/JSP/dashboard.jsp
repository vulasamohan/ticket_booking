<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 
     
    <title>train ticket booking login</title>
    
         <link href="/CSS/dashboard.css" type="text/css" rel="stylesheet" />
         
  <title>Dashboard page for local train ticket booking</title>
  </head>
  <body>
    <div class="Dashboard-content">
      <div class="navbar">
        <div class="logo">
          <a href="">
            <img
              src="/IMAGES/logo.png"
              class="irctc-logo"
              width="130px"
              height="auto"
          /></a>
        </div>
        <nav>
          <ul id="Menu">
            <li><a href="/user/home">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="/dashboard/contact">Contact Us</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="/dashboard/wallet">E-Ticket Wallet</a></li>
            <li><a href="#">Sign Out</a></li>
          </ul>
        </nav>
        <a href="/user/login"
          ><img
            src="/IMAGES/cart.png"
            alt="cart"
            width="90px"
            height="90px"
        /></a>
      </div>

      <div class="categories">
        <div class="small-container">
          <div class="row">
            <div class="col-3">
            
           <a href="/dashboard/book">
              <img
                src="/IMAGES/normalbooking.png"
                alt="Normal Booking"
              />
            </a>
              <h3>Normal Booking</h3>
            </div>
            
            <div class="col-3">
            <a href="/dashboard/bookinglist">
              <img
                src="/IMAGES/listof_booking.png"
                alt="List of Booking"
              />
              </a>
              <h3>List of Booking</h3>
            </div>
            
            <div class="col-3">
            
            <a href="/dashboard/cancel">
              <img
                src="/IMAGES/cancel ticket.jpg"
                alt="Cancel Ticket"
              />
              </a>
              <h3>Cancel Ticket</h3>
            </div>
            <div class="col-3">
             <a href="/dashboard/status">
              <img src="/IMAGES/trainstatus.png" alt="Live Train Status" />
                </a>
              <h3>Live Train Status</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>