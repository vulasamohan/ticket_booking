package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="train")
public class Train {
	
	@Id
	private int trainid;
	
	private String station1;
	
	private String station2;
	
	private String station3;
	
	private String station4;
	
	private String station5;
	
	private String d_a_t_e;
	
	public Train(){
	}
	
	

	public Train(int trainid, String station1, String station2, String station3, String station4, String station5,
			String d_a_t_e) {
		super();
		this.trainid = trainid;
		this.station1 = station1;
		this.station2 = station2;
		this.station3 = station3;
		this.station4 = station4;
		this.station5 = station5;
		this.d_a_t_e = d_a_t_e;
	}



	public int getTrainid() {
		return trainid;
	}

	public void setTrainid(int trainid) {
		this.trainid = trainid;
	}

	
	public String getStation1() {
		return station1;
	}

	public void setStation1(String station1) {
		this.station1 = station1;
	}



	public String getStation2() {
		return station2;
	}



	public void setStation2(String station2) {
		this.station2 = station2;
	}



	public String getStation3() {
		return station3;
	}



	public void setStation3(String station3) {
		this.station3 = station3;
	}



	public String getStation4() {
		return station4;
	}



	public void setStation4(String station4) {
		this.station4 = station4;
	}



	public String getStation5() {
		return station5;
	}



	public void setStation5(String station5) {
		this.station5 = station5;
	}

	public void setD_a_t_e(String d_a_t_e) {
		this.d_a_t_e = d_a_t_e;
	}

	public String getD_a_t_e() {
		return d_a_t_e;
	}


	

}