package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.ModelAndView;


import com.example.entity.Passenger;


@Repository
public interface PassengerRepository extends CrudRepository<Passenger,Integer> {

	public List<Passenger> findAll();
	
	public Optional<Passenger> findById(Integer userid);
	
	
    public boolean existsByEmail(@Param("email") String email);
	
	
    
	public Passenger save(Passenger passenger);
}